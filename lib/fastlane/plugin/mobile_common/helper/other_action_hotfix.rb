module Fastlane
  # This class is used to call other actions from within actions
  # We use a separate class so that we can easily identify when
  # we have dependencies between actions
  class OtherAction
    # Allows the user to call an action from an action
    def method_missing(method_sym, *arguments, &_block)
      # We have to go inside the fastlane directory
      # since in the fastlane runner.rb we do the following
      #   custom_dir = ".."
      #   Dir.chdir(custom_dir) do
      # this goes one folder up, since we're inside the "fastlane"
      # folder at that point
      # Since we call an action from an action we need to go inside
      # the fastlane folder too

      # FIXME: because of changes in fastlane runner - we can't just pass fastlane folder - action will be called in fastlane folder
      # for now this is a workaround - pass current folder, so fastlane will switch nowhere
      # FIXME: create an issue in official fastlane repo
      # Now fastlane runner switches to upper level folder only if nil was passed:
      # custom_dir ||= ".."

      self.runner.trigger_action_by_name(method_sym,
                                         '.',
                                         true,
                                         *arguments)
    end
  end
end
