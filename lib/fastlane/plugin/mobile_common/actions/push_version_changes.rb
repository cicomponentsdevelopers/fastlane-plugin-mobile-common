module Fastlane
  module Actions
    module SharedValues
    end

    class PushVersionChangesAction < Action
      def self.run(params)
        git_branch = params[:git_branch]

        # remove all files from index
        sh("git reset")

        other_action.commit_version_bump(
          force: true,
          message: params[:commit_message],
          xcodeproj: params[:xcodeproj]
        )
        other_action.push_to_git_remote(
          local_branch: git_branch
        )
      end

      #####################################################
      # @!group Documentation
      #####################################################

      def self.description
        "Commits target version changes to repository and pushes created commit"
      end

      def self.available_options
        [
          FastlaneCore::ConfigItem.new(key: :commit_message,
                                       env_name: "FL_PUSH_VERSION_CHANGES_COMMIT_MESSAGE",
                                       description: "Commit message for version bump",
                                       verify_block: proc do |value|
                                         UI.user_error!("No CommitMessage for PushVersionChangesAction given, pass using `commit_message: 'message'`") unless value and !value.empty?
                                       end),
          FastlaneCore::ConfigItem.new(key: :git_branch,
                                       env_name: "FL_PUSH_VERSION_CHANGES_GIT_BRANCH",
                                       description: "Branch to push changes",
                                       verify_block: proc do |value|
                                         UI.user_error!("No Git branch for PushVersionChangesAction given, pass using `git_branch: 'branch'`") unless value and !value.empty?
                                       end),
          FastlaneCore::ConfigItem.new(key: :xcodeproj,
                                       env_name: "FL_PUSH_VERSION_CHANGES_XCODEPROJ",
                                       description: "If you have multiple Xcode project files, you must specify your main project here",
                                       optional: true,
                                       verify_block: proc do |value|
                                         UI.user_error!("Please pass the path to the project, not the workspace") if value.end_with? ".xcworkspace"
                                         UI.user_error!("Could not find Xcode project at path '#{File.expand_path(value)}'") unless File.exist?(value)
                                       end)
        ]
      end

      def self.authors
        # So no one will ever forget your contribution to fastlane :) You are awesome btw!
        ["SemenovAlexander"]
      end

      def self.is_supported?(platform)
        [:ios, :mac].include?(platform)
      end
    end
  end
end
