module Fastlane
  module Actions
    module SharedValues
      DEPLOYED_BUILD_NUMBER = :DEPLOYED_BUILD_NUMBER
      NEW_BUILD_NUMBER = :NEW_BUILD_NUMBER
    end

    class IosPublishBetaAction < Action
      def self.run(params)
        # this is required for
        require 'fastlane/plugin/versioning'
        require 'fastlane/plugin/get_product_bundle_id'

        # read params
        git_branch = params[:git_branch]
        artifacts_directory = params[:artifacts_directory]
        build_targets = Array(params[:build_targets])
        build_target_config = Array(params[:build_config_name])
        build_targets_version_increment = Array(params[:build_targets_version_increment])
        build_targets_version_increment = build_targets if build_targets_version_increment.empty? || build_targets_version_increment.nil?

        app_suffix = params[:app_suffix]
        app_suffix = "" unless app_suffix # if suffix was not passed - use empty string
        xcodeproj = params[:xcodeproj]
        ipa_dir = params[:ipa_source_directory]

		distribute_external = params[:distribute_external]
		external_groups = Array(params[:groups])

        # removing old artifacts to be sure, we are building from scratch
        sh("mkdir -p #{artifacts_directory}") # first - we need to ensure, we have artifacts directory

        sh("rm -rf #{artifacts_directory}/*.*")

        # copy artifacts to for jenkins
        sh("cp -r *.ipa #{artifacts_directory}")
        sh("cp -r *.dSYM.zip #{artifacts_directory}")
       
        build_number = other_action.get_build_number_from_plist(
          target: build_targets.first,
          xcodeproj: xcodeproj,
          build_configuration_name: build_target_config.first
        )
        
        version_number = other_action.get_version_number_from_plist(
          target: build_targets.first,
          xcodeproj: xcodeproj,
          build_configuration_name: build_target_config.first
        )

        changelog_filename_suffix = "_#{app_suffix}" unless app_suffix.to_s == ''
        changelog = other_action.update_changelog(
          version: "#{version_number} (#{build_number})",
          current_log_filename: "CHANGELOG_CURRENT#{changelog_filename_suffix}.md",
          log_filename: "CHANGELOG#{changelog_filename_suffix}.md"
        )
        Actions.lane_context[SharedValues::UPDATE_CHANGELOG_CURRENT] = changelog

        # for now pilot doesn't see submitted binary even after processing is complete.
        # Probably some issue in TestFlight, so we deploy build to testers manually on each new version.

        build_targets.each do |build_target| 
          if build_target_config.count > 0 then
            # support builds from different build configurations
            build_target_config.each do |build_config|
              bundle_id = other_action.get_product_bundle_id(project_filepath: xcodeproj, scheme: build_target, target: build_config)
              ipa_path = File.join(Dir.pwd, ipa_dir, "#{build_target}-#{build_config}.ipa" )
              upload_ipa(ipa_path, bundle_id, changelog, distribute_external, external_groups)
            end
          else
            bundle_id = other_action.get_product_bundle_id(project_filepath: xcodeproj, scheme: build_target)
            ipa_path = File.join(Dir.pwd, ipa_dir, "#{build_target}.ipa")
            upload_ipa(ipa_path, bundle_id, changelog, distribute_external, external_groups)
          end
        end

        other_action.commit_changelog(
          app_suffix: app_suffix
        )

        git_tag_folder_prefix = app_suffix.downcase
        # add separator between prefix and `beta` word in tag folder if we have app suffix
        git_tag_folder_prefix += '_' unless git_tag_folder_prefix == ''
        other_action.add_git_tag(
          tag: "builds/#{git_tag_folder_prefix}beta/#{build_number}"
        )

        Actions.lane_context[SharedValues::DEPLOYED_BUILD_NUMBER] = build_number
        build_targets_version_increment.each do |build_target|
          other_action.increment_build_number_in_plist(
            xcodeproj: xcodeproj,
            target: build_target,
            build_configuration_name: build_target_config.first
          )
        end

        new_build_number = Actions.lane_context[SharedValues::BUILD_NUMBER]
        new_version_number = version_number
        Actions.lane_context[SharedValues::NEW_BUILD_NUMBER] = new_build_number

        other_action.push_version_changes(
          commit_message: "NO-TRACK Version Code Bump #{new_build_number} #{app_suffix} [ci skip]",
          xcodeproj: xcodeproj,
          git_branch: git_branch
        )
        # this action adds build number to props file for jenkins. this will be added as version to Jira
        sh "echo JIRA_NEXT_VERSION=#{new_version_number}.#{new_build_number} > fastlane/version.props"
        UI.important "Don't forget to check TestFlight build availability in Itunes Connect!!!"
      end

      def self.upload_ipa(ipa_path, bundle_id, changelog, distribute_external, external_groups)
        UI.message "Uploading ipa #{ipa_path} for bunlde id #{bundle_id}"
        other_action.pilot(
          skip_submission: false,
          ipa: ipa_path,
          skip_waiting_for_build_processing: false,
          changelog: changelog,
          app_identifier: bundle_id,
          distribute_external: distribute_external,
          groups: external_groups
        )
      end
      #####################################################
      # @!group Documentation
      #####################################################

      def self.output
        # Define the shared values you are going to provide
        # Example
        [
          ['UPDATE_CHANGELOG_CURRENT', 'Current version changelog string representation, you can send it to slack, for instance'],
          ['DEPLOYED_BUILD_NUMBER', 'Build number of build, which was deployed to ITunesConnect'],
          ['NEW_BUILD_NUMBER', 'Build number after increment']
        ]
      end

      def self.description
        "Builds new app version and deploys it to Apple TestFlight. Also updates app changelog."
      end

      def self.details
        "Action does following for each given target:
         * increment build number by 1
         * uploads newly built build to Apple TestFlight
         * copies current changelog to common one
         * commits version bump
         * pushes all changes

          IMPORTANT! You should build and archive app before calling this action."
      end

      def self.available_options
        [
          FastlaneCore::ConfigItem.new(key: :git_branch,
                                       env_name: "FL_IOS_PUBLISH_BETA_GIT_BRANCH",
                                       description: "Git branch to push back changelog and version updates",
                                       default_value: "master"),
          FastlaneCore::ConfigItem.new(key: :build_targets,
                                       env_name: "FL_IOS_PUBLISH_BETA_TARGETS",
                                       description: "Array of Xcode targets to publish. Project should have dedicated scheme for each target",
                                       type: Array,
                                       verify_block: proc do |value|
                                         UI.user_error!("XCode targets to publish not specified, pass using `build_targets: ['target_name1', 'target_name2']`") unless value and !value.empty?
                                       end),
          FastlaneCore::ConfigItem.new(key: :build_config_name,
                                       env_name: "FL_IOS_PUBLISH_BETA_TARGET_CONFIG_NAME",
                                       description: "One of the Xcode build configuration name. Set up this parameter if you use different plist for different build configurations",
                                       type: Array,
                                       optional: true),
          FastlaneCore::ConfigItem.new(key: :build_targets_version_increment,
                                       env_name: "FL_IOS_PUBLISH_BETA_TARGETS_VERSION_INCREMENT",
                                       description: "Array of Xcode targets to increment build number, if nil - build_targets will be used. Project should have dedicated scheme for each target",
                                       type: Array,
                                       optional: true,
                                       verify_block: proc do |value|
                                         UI.user_error!("XCode targets to increment build number not specified, pass using `build_targets: ['target_name1', 'target_name2']`") unless value and !value.empty?
                                       end),
          FastlaneCore::ConfigItem.new(key: :xcodeproj,
                                       env_name: "FL_IOS_PUBLISH_BETA_XCODEPROJ",
                                       description: "Path to Xcode project to find targets",
                                       verify_block: proc do |value|
                                         UI.user_error!("Please pass the path to the project, not the workspace") if value.end_with? ".xcworkspace"
                                         UI.user_error!("Could not find Xcode project at path '#{File.expand_path(value)}'") unless File.exist?(value)
                                       end),
          FastlaneCore::ConfigItem.new(key: :artifacts_directory,
                                       env_name: "FL_IOS_PUBLISH_BETA_ARTIFACTS_DIRECTORY",
                                       description: "Directory to store build artifacts like IPA files",
                                       default_value: "artifacts"),
          FastlaneCore::ConfigItem.new(key: :ipa_source_directory,
                                       env_name: "FL_IOS_PUBLISH_BETA_IPA_SOURCE_DIRECTORY",
                                       description: "Directory with IPA files for each target, which should be uploaded to itunesconnect. By default - root dir is used",
                                       default_value: ""),
          FastlaneCore::ConfigItem.new(key: :app_suffix,
                                       env_name: "FL_IOS_PUBLISH_BETA_APP_SUFFIX",
                                       description: "Suffix, added to changelog filenames, used to split changelogs for different app flavors in same repository",
                                       default_value: "",
                                       is_string: true,
                                       optional: true),
          FastlaneCore::ConfigItem.new(key: :distribute_external,
                                       env_name: "FL_IOS_PUBLISH_BETA_APP_DISTRIBUTE_EXTERNAL",
                                       description: "Should the build be distributed to external testers?. By default - false",
                                  	   is_string: false,
                                       default_value: false,
                                       optional: true),
          FastlaneCore::ConfigItem.new(key: :groups,
                                       env_name: "FL_IOS_PUBLISH_BETA_APP_GROUPS",
                                       description: "Names/ids of groups of external testers. By default - used one group 'External'",
                                       type: Array,
                                       default_value: ["External"],
                                       optional: true,
                                       verify_block: proc do |value|
                                         UI.user_error!("External test groups to assosiate testers not specified, pass using `groups: ['tester_group1', 'tester_group2']`") unless value and !value.empty?
                                       end)
        ]
      end

      def self.authors
        # So no one will ever forget your contribution to fastlane :) You are awesome btw!
        ["SemenovAlexander"]
      end

      def self.is_supported?(platform)
        platform == :ios
      end
    end
  end
end
