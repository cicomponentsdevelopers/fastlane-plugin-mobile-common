module Fastlane
  module Actions
    module SharedValues
    end

    class CommitChangelogAction < Action
      def self.run(params)
        # fastlane will take care of reading in the parameter and fetching the environment variable:
        app_suffix = params[:app_suffix]
        changelog_filename_suffix = "_#{app_suffix}" unless app_suffix.to_s == ''

        # 		ensure we have not staged anything except changelog
        sh("git reset HEAD")
        # 		adding changed files, we need to add uplevel because this code is executed inside 'fastlane' folder
        sh("git add CHANGELOG#{changelog_filename_suffix}.md")
        sh("git add CHANGELOG_CURRENT#{changelog_filename_suffix}.md")
        sh("git commit -m \"Changelog update #{app_suffix}\"")
      end

      #####################################################
      # @!group Documentation
      #####################################################

      def self.description
        "Commits changelog changes to git, which were made by 'update_changelog' action"
      end

      def self.available_options
        [
          FastlaneCore::ConfigItem.new(key: :app_suffix,
                                       env_name: "FL_COMMIT_CHANGELOG_APP_SUFFIX", # The name of the environment variable
                                       description: "Suffix, added to changelog filenames, used to split changelogs for different app flavors in same repository", # a short description of this parameter
                                       is_string: true,
                                       default_value: "",
                                       optional: true)
        ]
      end

      def self.authors
        # So no one will ever forget your contribution to fastlane :) You are awesome btw!
        ["SemenovAlexander"]
      end

      def self.is_supported?(platform)
        true
      end
    end
  end
end
