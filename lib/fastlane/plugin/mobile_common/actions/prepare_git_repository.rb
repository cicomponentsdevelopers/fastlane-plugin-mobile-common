module Fastlane
  module Actions
    module SharedValues
    end

    class PrepareGitRepositoryAction < Action
      def self.run(params)
        git_branch = params[:branch]
        remote = params[:remote]
        sh("git checkout #{git_branch}")
        sh("git remote set-branches --add #{remote} #{git_branch}")
        sh("git branch --set-upstream-to=refs/remotes/#{remote}/#{git_branch} #{git_branch}")
        other_action.git_pull
      end

      #####################################################
      # @!group Documentation
      #####################################################

      def self.description
        "Prepares repo for fastlane to work"
      end

      def self.details
        # Optional:
        # this is your chance to provide a more detailed description of this action
        "You can use this action to do cool things..."
      end

      def self.available_options
        [
          FastlaneCore::ConfigItem.new(key: :branch,
                                       env_name: "FL_PREPARE_GIT_REPOSITORY_BRANCH",
                                       description: "Git branch to checkout and track for rest of fastlane",
                                       default_value: "master",
                                       verify_block: proc do |value|
                                         UI.user_error!("No branch for PrepareGitRepositoryAction given, pass using `branch: 'branch'`") unless value and !value.empty?
                                       end),
          FastlaneCore::ConfigItem.new(key: :remote,
                                       env_name: "FL_PREPARE_GIT_REPOSITORY_REMOTE",
                                       description: "Git remote name to checkout and track for rest of fastlane",
                                       default_value: "origin",
                                       verify_block: proc do |value|
                                         UI.user_error!("No git remote for PrepareGitRepositoryAction given, pass using `remote: 'origin'`") unless value and !value.empty?
                                       end)
        ]
      end

      def self.authors
        # So no one will ever forget your contribution to fastlane :) You are awesome btw!
        ["SemenovAlexander"]
      end

      def self.is_supported?(platform)
        true
      end
    end
  end
end
