# mobile_common plugin

[![fastlane Plugin Badge](https://rawcdn.githack.com/fastlane/fastlane/master/fastlane/assets/plugin-badge.svg)](https://rubygems.org/gems/fastlane-plugin-mobile_common)

## Getting Started

This project is a [_fastlane_](https://github.com/fastlane/fastlane) plugin. To get started with `fastlane-plugin-mobile_common`, add it to your project by running:

```bash
fastlane add_plugin mobile_common
```

## About mobile_common

Collection of common CI actions for Android, iOS and MacOSX projects - like unit tests, app publishing, version increment, screenshots, etc.

**Note to author:** Add a more detailed description about this plugin here. If your plugin contains multiple actions, make sure to mention them here.

## Actions
* __update_changelog__ - gets contents of CHANGELOG_CURRENT.md and appends them to CHANGELOG.md, adding app version and current date
* __commit_changelog__ - commits and pushes to repo changes, made by `update_changelog` action
* __ios_publish_beta__ - publishes already built iOS binary to itunes connect, bumps version, performs much of other automation, see description
* __prepare_git_repository__ - pulls all changes from given branch and sets up local branch to track remote one. Useful for jenkins
* __push_version_changes__ - commits target version changes to repository and pushes created commit
* __prepare_snapfiles__ - creates individual Snapfile for each given device/locale combination, to let run snapshot on each of them individually, and have individual report 

## Example

Check out the [example `Fastfile`](fastlane/Fastfile) to see how to use this plugin. Try it by cloning the repo, running `fastlane install_plugins` and `bundle exec fastlane test`.

## Run tests for this plugin

To run both the tests, and code style validation, run

```
rake
```

To automatically fix many of the styling issues, use
```
rubocop -a
```

## Issues and Feedback

For any other issues and feedback about this plugin, please submit it to this repository.

## Troubleshooting

If you have trouble using plugins, check out the [Plugins Troubleshooting](https://docs.fastlane.tools/plugins/plugins-troubleshooting/) guide.

## Using _fastlane_ Plugins

For more information about how the `fastlane` plugin system works, check out the [Plugins documentation](https://docs.fastlane.tools/plugins/create-plugin/).

## About _fastlane_

_fastlane_ is the easiest way to automate beta deployments and releases for your iOS and Android apps. To learn more, check out [fastlane.tools](https://fastlane.tools).
