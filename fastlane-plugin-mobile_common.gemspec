# coding: utf-8

lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'fastlane/plugin/mobile_common/version'

Gem::Specification.new do |spec|
  spec.name          = 'fastlane-plugin-mobile_common'
  spec.version       = Fastlane::MobileCommon::VERSION
  spec.author        = 'Alexander Semenov'
  spec.email         = 'alexs@2kgroup.com'

  spec.summary       = 'Collection of common CI actions for Android, iOS and MacOSX projects - like unit tests, app publishing, version increment, screenshots, etc.'
  # spec.homepage      = "https://github.com/<GITHUB_USERNAME>/fastlane-plugin-mobile_common"
  spec.license       = "MIT"

  spec.files         = Dir["lib/**/*"] + %w(README.md LICENSE)
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  # Don't add a dependency to fastlane or fastlane_re
  # since this would cause a circular dependency

  # spec.add_dependency 'your-dependency', '~> 1.0.0'

  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'fastlane', '>= 2.38.1'

  spec.add_dependency 'fastlane-plugin-versioning'
  spec.add_dependency 'fastlane-plugin-get_product_bundle_id'
end
